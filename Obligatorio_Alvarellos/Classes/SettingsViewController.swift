//
//  SettingsViewController.swift
//  Obligatorio_Alvarellos
//
//  Created by Andoni Alvarellos on 5/20/16.
//  Copyright © 2016 Andoni Alvarellos. All rights reserved.
//

import UIKit
import MapKit

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    var unit: Int = 0
    var useCurrentLocation: Bool = true
    var fixedLongitude: Double = 0
    var fixedLatitude: Double = 0
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        table.tableFooterView = map
        let tapRecognizer:  UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(foundTap))
        map.addGestureRecognizer(tapRecognizer)
        
        let usedPoint: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: NSUserDefaults.standardUserDefaults().doubleForKey("FixedLatitude"), longitude: NSUserDefaults.standardUserDefaults().doubleForKey("FixedLongitude"))
        
        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = usedPoint
        map.addAnnotation(pointAnnotation)
        
        
        map.hidden = NSUserDefaults.standardUserDefaults().boolForKey("UseCurrentLocation")
    }
    
    func unitSegmentedControlChanged(sender: UISegmentedControl) {
        unit = sender.selectedSegmentIndex
    }
    
    func toggleMapSwitchChanged(sender: UISwitch){
        useCurrentLocation = sender.on
        map.hidden = useCurrentLocation
    }
    
    @IBAction func CancelPuttonPressed(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func SaveButtonPressed(sender: UIBarButtonItem) {
        
        NSUserDefaults.standardUserDefaults().setInteger(unit, forKey: "Unit")
        NSUserDefaults.standardUserDefaults().setBool(useCurrentLocation, forKey: "UseCurrentLocation")
        NSUserDefaults.standardUserDefaults().setDouble(fixedLatitude, forKey: "FixedLatitude")
        NSUserDefaults.standardUserDefaults().setDouble(fixedLongitude, forKey: "FixedLongitude")
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let unitCell = tableView.dequeueReusableCellWithIdentifier("TemperatureUnitSettingsCell", forIndexPath: indexPath) as! TemperatureUnitSettingsCell
            
            unitCell.selectionStyle = UITableViewCellSelectionStyle.None
            
            unitCell.UnitSegmentedControl.selectedSegmentIndex = NSUserDefaults.standardUserDefaults().integerForKey("Unit")
            
            unitCell.UnitSegmentedControl.addTarget(self, action: #selector(SettingsViewController.unitSegmentedControlChanged(_:)), forControlEvents: .ValueChanged)
            
            return unitCell
        } else {
            if indexPath.row == 1{
                let toggleMapCell = tableView.dequeueReusableCellWithIdentifier("ToggleMapSettingsCell", forIndexPath: indexPath) as! ToggleMapSettingsCell
                
                toggleMapCell.selectionStyle = UITableViewCellSelectionStyle.None
                
                toggleMapCell.toggleMapSwitch.on = NSUserDefaults.standardUserDefaults().boolForKey("UseCurrentLocation")
                
                toggleMapCell.toggleMapSwitch.addTarget(self, action: #selector(SettingsViewController.toggleMapSwitchChanged(_:)), forControlEvents: .ValueChanged)
                
                return toggleMapCell
            }
            
            return UITableViewCell()
        }
    }
    
    @IBAction func foundTap (recognizer: UITapGestureRecognizer) {
        
        map.removeAnnotations(map.annotations)
        
        let point: CGPoint = recognizer.locationInView(map)
        let tapPoint: CLLocationCoordinate2D  = map.convertPoint(point, toCoordinateFromView: map)
        
        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = tapPoint
        map.addAnnotation(pointAnnotation)
        
        self.fixedLatitude = tapPoint.latitude
        self.fixedLongitude = tapPoint.longitude
    }
}
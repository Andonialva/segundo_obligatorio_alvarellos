//
//  CollectionItemController.swift
//  Obligatorio_Alvarellos
//
//  Created by Andoni Alvarellos on 5/27/16.
//  Copyright © 2016 Andoni Alvarellos. All rights reserved.
//

import UIKit

public class CollectionItem: UICollectionViewCell{
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    
}
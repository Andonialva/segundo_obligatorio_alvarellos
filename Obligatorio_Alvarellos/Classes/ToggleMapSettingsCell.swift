//
//  ToggleMapSettingsCell.swift
//  Obligatorio_Alvarellos
//
//  Created by Andoni Alvarellos on 6/2/16.
//  Copyright © 2016 Andoni Alvarellos. All rights reserved.
//

import UIKit

public class ToggleMapSettingsCell: UITableViewCell{
    @IBOutlet weak var toggleMapSwitch: UISwitch!
}


//
//  TemperatureUnitSettingsCell.swift
//  Obligatorio_Alvarellos
//
//  Created by Andoni Alvarellos on 6/1/16.
//  Copyright © 2016 Andoni Alvarellos. All rights reserved.
//

import UIKit

public class TemperatureUnitSettingsCell: UITableViewCell{
    @IBOutlet weak var UnitSegmentedControl: UISegmentedControl!

}

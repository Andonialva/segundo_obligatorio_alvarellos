//
//  DayForecast.swift
//  Obligatorio_Alvarellos
//
//  Created by Andoni Alvarellos on 5/27/16.
//  Copyright © 2016 Andoni Alvarellos. All rights reserved.
//

struct DayForecast{
    var day: String!
    var icon: String!
    var temperature: String!
}

//
//  ViewController.swift
//  Obligatorio_Alvarellos
//
//  Created by Andoni Alvarellos on 5/18/16.
//  Copyright © 2016 Andoni Alvarellos. All rights reserved.
//
import Alamofire
import SwiftyJSON
import UIKit
import Foundation
import CoreLocation


class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, CLLocationManagerDelegate {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var nowIconLabel: UILabel!
    @IBOutlet weak var nowTemperatureLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var settingsButton: UIButton!
    
    let locationManager = CLLocationManager()
    var currentLongitude: Double = 45
    var currentLatitude: Double = -76
    var weekForecast: [DayForecast] = []
    
    @IBAction func settingsButtonPressed(sender: UIButton) {
        performSegueWithIdentifier("toSettingsViewController", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
    }
    
    override func viewDidAppear(animated: Bool) {
        self.locationManager.requestWhenInUseAuthorization()
        
        if !NSUserDefaults.standardUserDefaults().boolForKey("UseCurrentLocation"){
            currentLatitude = NSUserDefaults.standardUserDefaults().doubleForKey("FixedLatitude")
            currentLongitude = NSUserDefaults.standardUserDefaults().doubleForKey("FixedLongitude")
            self.loadingActivityIndicator.stopAnimating()
            self.settingsButton.hidden = false
            requestForecast()
        }else{
            if CLLocationManager.locationServicesEnabled() {
                self.locationManager.delegate = self;
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.startUpdatingLocation()
            }
        }
    }
    
    func requestForecast(){
        var unit:String=""
        var unitSign:String=""
        if (NSUserDefaults.standardUserDefaults().integerForKey("Unit") == 0){
            unit = "metric"
            unitSign = "ºC"
        }else{
            unit = "imperial"
            unitSign = "ºF"
        }
        
        
        
        Alamofire.request(.GET, "http://api.openweathermap.org/data/2.5/forecast/daily?lat="+currentLatitude.description+"&lon="+currentLongitude.description+"&cnt=16&units="+unit+"&APPID=135116a3b31cb7be71d9b9f89de59cbd").validate().responseJSON { response in
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    self.weekForecast=[]
                    
                    let json = JSON(value)
                    let forecast = json["list"]
                    
                    let cityName = json["city"]["name"].stringValue
                    let temperature = Int(round(forecast[0]["temp"]["day"].floatValue)).description
                    let todayWeather = forecast[0]["weather"][0]
                    
                    let iconCondition = todayWeather["id"].intValue
                    let iconString = todayWeather["icon"].stringValue
                    
                    for label in [self.cityLabel, self.nowTemperatureLabel, self.nowIconLabel]{
                        label.hidden = false
                    }
                    
                    self.cityLabel.text = cityName
                    self.nowTemperatureLabel.text = temperature + unitSign
                    let icon: WeatherIcon =  WeatherIcon(condition: iconCondition, iconString: iconString)
                    self.nowIconLabel.text = icon.iconText
                    
                    for dayIndex in 1...6{
                        
                        let currentWeekDay = self.getCurrentWeekDay(dayIndex)
                        let currentDayWeather = forecast[dayIndex]["weather"][0]
                        let currentTemperature = Int(round(forecast[dayIndex]["temp"]["day"].floatValue)).description
                        
                        let currentIconCondition = currentDayWeather["id"].intValue
                        let currentIconString = currentDayWeather["icon"].stringValue
                        
                        let dayIcon: WeatherIcon =  WeatherIcon(condition: currentIconCondition, iconString: currentIconString)
                        
                        self.weekForecast.append(DayForecast(day: currentWeekDay, icon: dayIcon.iconText, temperature: currentTemperature + unitSign))
                    }
                    
                    self.collectionView.reloadData()
                }
            case .Failure(let error):
                print(error)
            }
        }
    }
    
    func getCurrentWeekDay (plusDay: Int) -> String{
        let todayDate = NSDate()
        
        let currentDay = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: plusDay, toDate: todayDate, options: NSCalendarOptions.init(rawValue: 0))
        
        let formatter  = NSDateFormatter()
        
        formatter.dateFormat = "E"
        
        return formatter.stringFromDate(currentDay!)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weekForecast.count
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let collectionItem = collectionView.dequeueReusableCellWithReuseIdentifier("weatherCell", forIndexPath: indexPath) as! CollectionItem
        
        for label in [collectionItem.dayLabel, collectionItem.iconLabel, collectionItem.temperatureLabel]{
            label.hidden = false
        }
        
        let currentForecast = weekForecast[indexPath.row]
        collectionItem.dayLabel.text = currentForecast.day
        collectionItem.iconLabel.text = currentForecast.icon
        collectionItem.temperatureLabel.text = currentForecast.temperature
        return collectionItem
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0]
        self.currentLongitude = userLocation.coordinate.longitude;
        self.currentLatitude = userLocation.coordinate.latitude;
        
        self.settingsButton.hidden = false
        self.loadingActivityIndicator.stopAnimating()
        requestForecast()
    }
    
    
}

